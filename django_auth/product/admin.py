from django.contrib import admin

# Register your models here.

from .models import Product, Category_product

admin.site.register(Product)
admin.site.register(Category_product)
