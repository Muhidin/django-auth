from os import name
from urllib.request import urlopen
from django.urls import path
from . import views


urlpatterns = [
    path('list', views.ProductListView.as_view(), name='product-list'),
    path('create', views.ProductCreateView.as_view(), name='product-create'),
    path('detail/<int:pk>', views.ProductDetailView.as_view(), name='product-detail'),
    path('delete/<int:pk>', views.ProductDeleteView.as_view(), name='product-delete'),
]