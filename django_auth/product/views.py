import imp
from re import template
from django.shortcuts import render

from django.contrib.auth.decorators import login_required
from django.template import context
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView
from django.views.generic import ListView, DetailView, DeleteView, UpdateView
from .models import Product, Category_product
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

from django.urls import reverse_lazy
# Create your views here.

@method_decorator(login_required, name='dispatch')
class ProductCreateView(CreateView):
    model = Product
    fields = '__all__'
    template_name = 'product/create.html'
    success_url = reverse_lazy('product-list')


@method_decorator(login_required, name='dispatch')
class ProductListView(ListView):

    model = Product
    template_name = 'product/list.html'

    context_object_name = 'products'
    paginate_by = 2

    def get_context_data(self, **kwargs):
        context = super(ProductListView, self).get_context_data(**kwargs)
        products = self.get_queryset()
        page = self.request.GET.get('page')
        paginator = Paginator(products, self.paginate_by)

        try:
            products = paginator.page(page)
        except PageNotAnInteger:
            products = paginator.page(1)
        except EmptyPage:
            products = paginator.page(paginator.num_pages)
        context['products'] = products
        return context

# handle detail
@method_decorator(login_required, name='dispatch')
class ProductDetailView(DetailView):
    model = Product
    template_name = 'product/detail.html'
    context_object_name = 'product'

@method_decorator(login_required, name='dispatch')
class ProductDeleteView(DeleteView):
    model = Product
    template_name = 'product/delete.html'
    success_url = reverse_lazy('product-list')

@method_decorator(login_required, name='dispatch')
class ProductUpdateView(UpdateView):
    model = Product
    
