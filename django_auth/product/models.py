from django.db import models

# Create your models here.

class Category_product(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        ordering = ['name']
    
    def __str__(self):
        return self.name


class Product(models.Model):
    code = models.CharField(max_length=10)
    name = models.CharField(max_length=100)
    price = models.IntegerField(default=0)
    stock = models.IntegerField(default=0)
    unit = models.CharField(max_length=15)
    category = models.ForeignKey(Category_product, on_delete=models.CASCADE)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name





